# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- ## Unreleased -->
<!-- ### Added -->
<!-- ### Bug Fixes -->
<!-- ### Changed -->

## 4.1.0
### Added
- Validate UUID

## 4.0.1
### Added
- Allow standard constructors to be used as a type

### Changed
- Update dependencies

## 4.0.0
### Changed
- String class constructor for type validation
- Number class constructor for type validation
- Date class constructor for type validation
- Array class constructor for type validation
- Boolean class constructor for type validation
- Object class constructor for type validation

## 3.0.0
### Changed
- Made a CommonJS module by default
- ToShortError -> TooShortError
- ToLongError -> TooLongError

## 2.0.1
### Added
- "*" and "any" types

## 2.0.0
### Changed
- :exclamation: Library name changed from `@langlois/form-validation` to `@langlois/validator` :exclamation:

## 0.2.0
### Added
- ability to validate more than one type for a single input

## 0.1.0
### Added
- validate form function that can validate one or more of the following rules:
-- required data is not undefined
-- data type matches its expected type
-- the data's length is greater than or equal to the minimum length
-- the data's length is less than or equal to the maximum length
-- the data is included in a list of allowed values
