const isProduction = () => process.env.NODE_ENV === `production`;

module.exports = {
	env: {
		es2021: true,
		node: true
	},
	extends: [
		`eslint:recommended`
	],
	overrides: [{
		env: { jest: true },
		files: [`*.spec.js`, `./src/**/__mocks__/**/*.js`],
		rules: {
			'no-unused-expressions': 0
		}
	}],
	parserOptions: {
		ecmaVersion: 2021
	},
	root: true,
	rules: {
		indent: [`error`, `tab`],
		'no-console': isProduction() ? `error` : `off`,
		'no-debugger': isProduction() ? `error` : `off`,
		'no-tabs': [`error`, {
			allowIndentationTabs: true
		}],
		quotes: [`error`, `backtick`],
		'require-atomic-updates': `off`,
		semi: [`error`, `always`]
	}
}
