const { validator } = require(`./index.js`);

describe(`validator`, () => {
	test(`exists`, () => {
		expect(validator).toBeDefined();
	});

	test(`is an Object`, () => {
		expect(validator).toBeInstanceOf(Object);
	});

	describe(`validate object function`, () => {
		test(`exists`, () => {
			expect(validator.validateObject).toBeDefined();
		});

		test(`is a Function`, () => {
			expect(validator.validateObject).toBeInstanceOf(Function);
		});
	});
});
