const { TooShortError, TooLongError } = require(`../error.js`);

/**
 * Throws a too short error if a field is less than the allowed minimum length.
 *
 * @param {string} input - The name of the input.
 * @param {number} minLength - A value > 0 that represents the minimum length that the data must be.
 * @param {*} data - The submitted data associated with the input.
 *
 * @throws {TooShortError} - The field is less than the allowed minimum length.
 *
 * @example
 *
 * try {
 *     isValueTooShort(`inputName`, 3, `abc`);
 * } catch (error) {
 *     // handle error if the input is shorter than the minimum allowed length
 * }
 */
module.exports.isValueTooShort = (input, minLength, data) => {
	if (minLength && data.length < minLength) {
		throw new TooShortError(`The ${input} field should be at least ${minLength} characters long.`);
	}
};

/**
 * Throws a too long error if a field is more than the allowed maximum length.
 *
 * @param {string} input - The name of the input.
 * @param {number} maxLength - A value > 0 that represents the maximum length that the data must be.
 * @param {*} data - The submitted data associated with the input.
 *
 * @throws {TooLongError} - The field is less than the allowed maximum length.
 *
 * @example
 *
 * try {
 *     isValueTooLong(`inputName`, 16, `abc`);
 * } catch (error) {
 *     // handle error if the input is longer than the maximum allowed length
 * }
 */
module.exports.isValueTooLong = (input, maxLength, data) => {
	if (maxLength && data.length > maxLength) {
		throw new TooLongError(`The ${input} field can't be longer than ${maxLength} characters.`);
	}
};
