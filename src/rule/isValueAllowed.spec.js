const { isValueAllowed } = require(`./isValueAllowed.js`);

describe(`is value allowed function`, () => {
	let allowedValues;
	let data;

	beforeEach(() => {
		allowedValues = [`abc`, `def`, `xyz`];
	});

	test(`exists`, () => {
		expect(isValueAllowed).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isValueAllowed).toBeInstanceOf(Function);
	});

	describe(`if the input type is not one of the enum values`, () => {
		beforeEach(() => {
			data = `completely incorrect`;
		});

		test(`throw an invalid type error`, () => {
			expect(() => isValueAllowed(`testInput`, allowedValues, data)).toThrow(
				new TypeError(`Field testInput has the value "completely incorrect" but its value should be one of abc, def, xyz.`)
			);
		});
	});

	describe(`if the input type is a one of the enum values`, () => {
		beforeEach(() => {
			data = `xyz`;
		});

		test(`don't throw an invalid type error`, () => {
			expect(() => isValueAllowed(`testInput`, allowedValues, data)).not.toThrow(
				TypeError
			);
		});
	});
});
