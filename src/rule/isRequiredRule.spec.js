const { RequiredError } = require(`../error.js`);
const { checkIsRequired } = require(`./isRequiredRule.js`);

describe(`check if the input is required rule`, () => {
	test(`exists`, () => {
		expect(checkIsRequired).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(checkIsRequired).toBeInstanceOf(Function);
	});

	describe(`if the input is required`, () => {
		describe(`the input is defined`, () => {
			test(`don't throw a RequiredError`, () => {
				expect(() => {
					return checkIsRequired(`testInput`, true, `hello`);
				}).not.toThrow(
					RequiredError
				);
			});
		});

		describe(`the input is not defined`, () => {
			test(`throw a RequiredError`, () => {
				expect(() => {
					return checkIsRequired(`testInput`, true, undefined);
				}).toThrow(
					new RequiredError(`Missing required field: testInput.`)
				);
			});
		});
	});

	describe(`if the input is not required`, () => {
		describe(`the input is defined`, () => {
			test(`don't throw a RequiredError`, () => {
				expect(() => {
					return checkIsRequired(`testInput`, false, `hello`);
				}).not.toThrow(
					RequiredError
				);
			});
		});

		describe(`the input is not defined`, () => {
			test(`don't throw a RequiredError`, () => {
				expect(() => {
					return checkIsRequired(`testInput`, false, undefined);
				}).not.toThrow(
					RequiredError
				);
			});
		});
	});
});
