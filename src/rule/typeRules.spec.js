jest.mock(`./typing/checkArrayType.js`, () => ({
	__esmodule: true,
	checkArrayType: jest.fn()
}));

jest.mock(`./typing/checkBooleanType.js`, () => ({
	__esmodule: true,
	checkBooleanType: jest.fn()
}));

jest.mock(`./typing/checkDateType.js`, () => ({
	__esmodule: true,
	checkDateType: jest.fn()
}));

jest.mock(`./typing/checkDateTimeType.js`, () => ({
	__esmodule: true,
	checkDateTimeType: jest.fn()
}));

jest.mock(`./typing/checkEmailType.js`, () => ({
	__esmodule: true,
	checkEmailType: jest.fn()
}));

jest.mock(`./typing/checkNumberType.js`, () => ({
	__esmodule: true,
	checkNumberType: jest.fn()
}));

jest.mock(`./typing/checkObjectType.js`, () => ({
	__esmodule: true,
	checkObjectType: jest.fn()
}));

jest.mock(`./typing/checkStringType.js`, () => ({
	__esmodule: true,
	checkStringType: jest.fn()
}));

jest.mock(`./typing/checkUuidType.js`, () => ({
	__esmodule: true,
	checkUuidType: jest.fn()
}));

const { validateTypeRules } = require(`./typeRules.js`);
const { checkArrayType } = require(`./typing/checkArrayType.js`);
const { checkBooleanType } = require(`./typing/checkBooleanType.js`);
const { checkDateType } = require(`./typing/checkDateType.js`);
const { checkDateTimeType } = require(`./typing/checkDateTimeType.js`);
const { checkEmailType } = require(`./typing/checkEmailType.js`);
const { checkNumberType } = require(`./typing/checkNumberType.js`);
const { checkObjectType } = require(`./typing/checkObjectType.js`);
const { checkStringType } = require(`./typing/checkStringType.js`);
const { checkUuidType } = require(`./typing/checkUuidType.js`);

describe(`validate if an input matches its expected type`, () => {
	let data;
	let input;

	beforeEach(() => {
		data = `fake data`;
		input = `testRule`;
	});

	describe.each(Object.entries({
		array: {
			func: checkArrayType,
			types: [
				Array,
				`arr`,
				`array`,
				`list`
			]
		},
		boolean: {
			func: checkBooleanType,
			types: [
				Boolean,
				`bool`,
				`boolean`
			]
		},
		date: {
			func: checkDateType,
			types: [ Date, `date` ]
		},
		datetime: {
			func: checkDateTimeType,
			types: [ `datetime`, `dt` ]
		},
		email: {
			func: checkEmailType,
			types: [ `email` ]
		},
		number: {
			func: checkNumberType,
			types: [
				Number,
				`int`,
				`integer`,
				`num`,
				`number`
			]
		},
		object: {
			func: checkObjectType,
			types: [
				Object,
				`obj`,
				`object`
			]
		},
		string: {
			func: checkStringType,
			types: [
				String,
				`str`,
				`string`,
				`text`,
				`txt`
			]
		},
		uuid: {
			func: checkUuidType,
			types: [ `uuid` ]
		}
	}))(`%s`, (key, { func, types }) => {
		test.each(types)(`call the proper function when type set to "%s"`, type => {
			validateTypeRules(input, type, data);

			expect(func).toHaveBeenCalledTimes(1);
			expect(func).toHaveBeenCalledWith(`testRule`, `fake data`);
		});
	});

	test.each([
		`enum`,
		`*`,
		`any`
	])(`when type "%s" doesn't throw an error`, type => {
		expect(() => validateTypeRules(input, type, data)).not.toThrow();
	});

	test.each([`wrong`, `123`])(`if type "%s" throw an unknown type error`, type => {
		expect(() => validateTypeRules(input, type, data)).toThrow(
			new TypeError(`Unknown type.`)
		);
	});
});
