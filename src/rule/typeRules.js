const { checkArrayType } = require(`./typing/checkArrayType.js`);
const { checkBooleanType } = require(`./typing/checkBooleanType.js`);
const { checkDateType } = require(`./typing/checkDateType.js`);
const { checkDateTimeType } = require(`./typing/checkDateTimeType.js`);
const { checkEmailType } = require(`./typing/checkEmailType.js`);
const { checkNumberType } = require(`./typing/checkNumberType.js`);
const { checkObjectType } = require(`./typing/checkObjectType.js`);
const { checkStringType } = require(`./typing/checkStringType.js`);
const { checkUuidType } = require(`./typing/checkUuidType.js`);
const { isA } = require(`@langlois/is-a`);

/**
 * A list of data types that the function tests for.
 * @typedef { 'text' | 'string' | 'int' | 'integer' | 'num' | 'number' | 'date' | 'datetime' | 'enum' | 'array' | 'list' | 'email' | 'bool' | 'boolean' | 'obj' | 'object' } DataType
 */

/**
 * Throws a type error if a field doesn't match it's expected value.
 *
 * @param {string} input - The name of the input.
 * @param {DataType} type - The data type that the input should be.
 * @param {*} data - The submitted data associated with the input.
 *
 * @throws {TypeError} - The type is unknown || The data's type doesn't match it's expected type.
 *
 * @example
 *
 * try {
 *     validateTypeRules(`inputName`, `string`, `some data`);
 * } catch (error) {
 *     // handle error if the input is not an instance of a String
 * }
 */
module.exports.validateTypeRules = (input, type, data) => {
	const typeToCheck = isA.string(type) ? type.toLowerCase() : type;

	switch (typeToCheck) {
	case String:
	case `str`:
	case `string`:
	case `text`:
	case `txt`:
		checkStringType(input, data);
		break;

	case Number:
	case `int`:
	case `integer`:
	case `num`:
	case `number`:
		checkNumberType(input, data);
		break;

	case Date:
	case `date`:
		checkDateType(input, data);
		break;

	case `datetime`:
	case `dt`:
		checkDateTimeType(input, data);
		break;

	case `enum`:
		break;

	case Array:
	case `arr`:
	case `array`:
	case `list`:
		checkArrayType(input, data);
		break;

	case `email`:
		checkEmailType(input, data);
		break;

	case Boolean:
	case `bool`:
	case `boolean`:
		checkBooleanType(input, data);
		break;

	case Object:
	case `obj`:
	case `object`:
		checkObjectType(input, data);
		break;

	case `uuid`:
		checkUuidType(input, data);
		break;

	case `*`:
	case `any`:
		break;

	default:
		throw new TypeError(`Unknown type.`);
	}
};
