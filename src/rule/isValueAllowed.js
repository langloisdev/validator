/**
 * Throws a type error if a field isn't part of a list of allowed values.
 *
 * @param {String} input - The name of the input.
 * @param {Array} allowedValues - The enum, a list of allowed values.
 * @param {*} data - The submitted data associated with the input.
 *
 * @throws {TypeError} - The field is not included in the list.
 *
 * @example
 *
 * try {
 *     isValueAllowed(`inputName`, [`abc`, `def`, `xyz`], `abc`);
 * } catch (error) {
 *     // handle error if the input isn't one of the allowed values
 * }
 */
module.exports.isValueAllowed = (input, allowedValues, data) => {
	if (!allowedValues.includes(data)) {
		throw new TypeError(`Field ${input} has the value "${data}" but its value should be one of ${allowedValues.join(`, `)}.`);
	}
};
