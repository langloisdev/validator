const { isA } = require(`@langlois/is-a`);

/**
 * Throws a type error if a field is to be an email and isn't.
 *
 * @param {string} input - The name of the input.
 * @param {*} data - The submitted data associated with the input.
 *
 * @throws {TypeError} - The field is not an email.
 *
 * @example
 *
 * try {
 *     checkEmailType(`inputName`, `francis@langlois.dev`);
 * } catch (error) {
 *     // handle error if the input is not a valid email
 * }
 */
module.exports.checkEmailType = (input, data) => {
	if (!isA.email(data)) {
		throw new TypeError(`Field ${input} should be a valid email. Received "${data}".`);
	}
};
