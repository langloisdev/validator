const { isA } = require(`@langlois/is-a`);

/**
 * Throws a type error if a field is to be an instance of a Boolean and isn't.
 *
 * @param {string} input - The name of the input.
 * @param {*} data - The submitted data associated with the input.
 *
 * @throws {TypeError} - The field is not of type Boolean.
 *
 * @example
 *
 * try {
 *     checkBooleanType(`inputName`, true);
 * } catch (error) {
 *     // handle error if the input is not an instance of a Boolean
 * }
 */
module.exports.checkBooleanType = (input, data) => {
	if (!isA.boolean(data)) {
		throw new TypeError(`Field ${input} should be an instance of a Boolean. Received "${data}".`);
	}
};
