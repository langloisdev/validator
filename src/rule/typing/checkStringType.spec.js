const { checkStringType } = require(`./checkStringType`);
const { isA } = require(`@langlois/is-a`);

describe(`checkStringType function`, () => {
	beforeEach(() => {
		isA.string = jest.fn(() => true);
	});

	test(`exists`, () => {
		expect(checkStringType).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(checkStringType).toBeInstanceOf(Function);
	});

	test(`doesn't throw an error if the data passed is an instance of a String`, () => {
		expect(() => {
			checkStringType(`fake-input`, `string`);
		}).not.toThrow();
	});

	describe(`the data passed is not an instance of a String`, () => {
		beforeEach(() => {
			isA.string.mockReturnValue(false);
		});

		test(`throws a type error`, () => {
			expect(() => {
				checkStringType(`fake-input`, 123);
			}).toThrow(TypeError);
		});

		test(`the error message is relevant`, () => {
			expect(() => {
				checkStringType(`fake-input`, 123);
			}).toThrow(`Field fake-input should be an instance of a String. Received "123".`);
		});
	});
});
