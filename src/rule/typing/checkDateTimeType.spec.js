const { checkDateTimeType } = require(`./checkDateTimeType`);
const { isA } = require(`@langlois/is-a`);

describe(`checkDateTimeType function`, () => {
	beforeEach(() => {
		isA.number = jest.fn(() => true);
	});

	test(`exists`, () => {
		expect(checkDateTimeType).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(checkDateTimeType).toBeInstanceOf(Function);
	});

	test(`doesn't throw an error if the data passed is an instance of a Datetime`, () => {
		expect(() => {
			checkDateTimeType(`fake-input`, new Date());
		}).not.toThrow();
	});

	describe(`the data passed is not an instance of a Datetime`, () => {
		beforeEach(() => {
			isA.number.mockReturnValue(false);
		});

		test(`throws a type error`, () => {
			expect(() => {
				checkDateTimeType(`fake-input`, `fake-date`);
			}).toThrow(TypeError);
		});

		test(`the error message is relevant`, () => {
			expect(() => {
				checkDateTimeType(`fake-input`, `fake-date`);
			}).toThrow(`Field fake-input should be an instance of a Datetime. Received "fake-date".`);
		});
	});
});
