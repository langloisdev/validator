const { isA } = require(`@langlois/is-a`);

/**
 * Throws a type error if a field is to be an instance of a UUID and isn't.
 *
 * @param {string} input - The name of the input.
 * @param {*} data - The submitted data associated with the input.
 *
 * @throws {TypeError} - The field is not a UUID.
 *
 * @example
 *
 * try {
 *     checkUuidType(`inputName`, `abc`);
 * } catch (error) {
 *     // handle error if the input is not an instance of a String
 * }
 */
module.exports.checkUuidType = (input, data) => {
	if (!isA.uuid(data)) {
		throw new TypeError(`Field ${input} should be an instance of a UUID. Received "${data}".`);
	}
};
