const { checkArrayType } = require(`./checkArrayType`);
const { isA } = require(`@langlois/is-a`);

describe(`checkArrayType function`, () => {
	beforeEach(() => {
		isA.array = jest.fn(() => true);
	});

	test(`exists`, () => {
		expect(checkArrayType).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(checkArrayType).toBeInstanceOf(Function);
	});

	test(`doesn't throw an error if the data passed is an instance of an Array`, () => {
		expect(() => {
			checkArrayType(`fake-input`, []);
		}).not.toThrow();
	});

	describe(`the data passed is not an instance of an Array`, () => {
		beforeEach(() => {
			isA.array.mockReturnValue(false);
		});

		test(`throws a type error`, () => {
			expect(() => {
				checkArrayType(`fake-input`, `[]`);
			}).toThrow(TypeError);
		});

		test(`the error message is relevant`, () => {
			expect(() => {
				checkArrayType(`fake-input`, `[]`);
			}).toThrow(`Field fake-input should be an instance of an Array. Received "[]".`);
		});
	});
});
