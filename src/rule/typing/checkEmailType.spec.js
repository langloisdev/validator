const { checkEmailType } = require(`./checkEmailType`);
const { isA } = require(`@langlois/is-a`);

describe(`checkEmailType function`, () => {
	beforeEach(() => {
		isA.email = jest.fn(() => true);
	});

	test(`exists`, () => {
		expect(checkEmailType).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(checkEmailType).toBeInstanceOf(Function);
	});

	test(`doesn't throw an error if the data passed is an email`, () => {
		expect(() => {
			checkEmailType(`fake-input`, `admin@langlois.dev`);
		}).not.toThrow();
	});

	describe(`the data passed is not an email`, () => {
		beforeEach(() => {
			isA.email.mockReturnValue(false);
		});

		test(`throws a type error`, () => {
			expect(() => {
				checkEmailType(`fake-input`, `fake-email`);
			}).toThrow(TypeError);
		});

		test(`the error message is relevant`, () => {
			expect(() => {
				checkEmailType(`fake-input`, `fake-email`);
			}).toThrow(`Field fake-input should be a valid email. Received "fake-email".`);
		});
	});
});
