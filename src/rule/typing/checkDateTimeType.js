const { isA } = require(`@langlois/is-a`);

/**
 * Throws a type error if a field is to be a Datetime and isn't.
 *
 * @param {string} input - The name of the input.
 * @param {*} data - The submitted data associated with the input.
 *
 * @throws {TypeError} - The field is not a Datetime.
 *
 * @example
 *
 * try {
 *     checkDateTimeType(`inputName`, new Date());
 * } catch (error) {
 *     // handle error if the input is not a valid DateTime
 * }
 */
module.exports.checkDateTimeType = (input, data) => {
	if (!isA.number(Date.parse(data))) {
		throw new TypeError(`Field ${input} should be an instance of a Datetime. Received "${data}".`);
	}
};
