const { isA } = require(`@langlois/is-a`);

/**
 * Throws a type error if a field is to be an instance of a Number and isn't.
 *
 * @param {string} input - The name of the input.
 * @param {*} data - The submitted data associated with the input.
 *
 * @throws {TypeError} - The field is not of type Number.
 *
 * @example
 *
 * try {
 *     checkNumberType(`inputName`, 123);
 * } catch (error) {
 *     // handle error if the input does not evaluate to an instance of a Number
 * }
 */
module.exports.checkNumberType = (input, data) => {
	if (!isA.number(data)) {
		const invalidData = isA.null(data) || isA.undefined(data);

		if (!invalidData && data.length > 0) {
			const castToNumber = 1 * data;

			if (isA.number(castToNumber)) return;
		}

		throw new TypeError(`Field ${input} should be an instance of a Number. Received "${data}".`);
	}
};
