const { isA } = require(`@langlois/is-a`);

/**
 * Throws a type error if a field is to be an instance of a String and isn't.
 *
 * @param {string} input - The name of the input.
 * @param {*} data - The submitted data associated with the input.
 *
 * @throws {TypeError} - The field is not of type String.
 *
 * @example
 *
 * try {
 *     checkStringType(`inputName`, `abc`);
 * } catch (error) {
 *     // handle error if the input is not an instance of a String
 * }
 */
module.exports.checkStringType = (input, data) => {
	if (!isA.string(data)) {
		throw new TypeError(`Field ${input} should be an instance of a String. Received "${data}".`);
	}
};
