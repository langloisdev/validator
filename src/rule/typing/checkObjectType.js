const { isA } = require(`@langlois/is-a`);

/**
 * Throws a type error if a field is to be an instance of a Object and isn't.
 *
 * @param {string} input - The name of the input.
 * @param {*} data - The submitted data associated with the input.
 *
 * @throws {TypeError} - The field is not of type Object.
 *
 * @example
 *
 * try {
 *     checkObjectType(`inputName`, {});
 * } catch (error) {
 *     // handle error if the input is not an instance of an Object
 * }
 */
module.exports.checkObjectType = (input, data) => {
	if (!isA.object(data)) {
		throw new TypeError(`Field ${input} should be an instance of an Object. Received "${data}".`);
	}
};
