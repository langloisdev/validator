const { checkObjectType } = require(`./checkObjectType`);
const { isA } = require(`@langlois/is-a`);

describe(`checkObjectType function`, () => {
	beforeEach(() => {
		isA.object = jest.fn(() => true);
	});

	test(`exists`, () => {
		expect(checkObjectType).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(checkObjectType).toBeInstanceOf(Function);
	});

	test(`doesn't throw an error if the data passed is an instance of an Object`, () => {
		expect(() => {
			checkObjectType(`fake-input`, {});
		}).not.toThrow();
	});

	describe(`the data passed is not an instance of an Object`, () => {
		beforeEach(() => {
			isA.object.mockReturnValue(false);
		});

		test(`throws a type error`, () => {
			expect(() => {
				checkObjectType(`fake-input`, `123abc`);
			}).toThrow(TypeError);
		});

		test(`the error message is relevant`, () => {
			expect(() => {
				checkObjectType(`fake-input`, `123abc`);
			}).toThrow(`Field fake-input should be an instance of an Object. Received "123abc".`);
		});
	});
});
