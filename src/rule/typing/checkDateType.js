const { isA } = require(`@langlois/is-a`);
const moment = require(`moment`);

/**
 * Throws a type error if a field is to be an instance of a Date and isn't.
 *
 * @param {string} input - The name of the input.
 * @param {*} data - The submitted data associated with the input.
 *
 * @throws {TypeError} - The field is not of type Date.
 *
 * @example
 *
 * try {
 *     checkDateType(`inputName`, new Date());
 * } catch (error) {
 *     // handle error if the input is not an instance of a Date
 * }
 */
module.exports.checkDateType = (input, data) => {
	const invalidInput = isA.null(data) || isA.undefined(data);

	if (invalidInput || (!isA.date(data) && !moment(new Date(data)).isValid())) {
		throw new TypeError(`Field ${input} should be an instance of a Date. Received "${data}".`);
	}
};
