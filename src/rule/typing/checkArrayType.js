const { isA } = require(`@langlois/is-a`);

/**
 * Throws a type error if a field is to be an instance of an Array and isn't.
 *
 * @param {string} input - The name of the input.
 * @param {*} data - The submitted data associated with the input.
 *
 * @throws {TypeError} - The field is not of type Array.
 *
 * @example
 *
 * try {
 *     checkArrayType(`inputName`, []);
 * } catch (error) {
 *     // handle error if the input is not an instance of an Array
 * }
 */
module.exports.checkArrayType = (input, data) => {
	if (!isA.array(data)) {
		throw new TypeError(`Field ${input} should be an instance of an Array. Received "${data}".`);
	}
};
