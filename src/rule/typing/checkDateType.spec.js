const { checkDateType } = require(`./checkDateType`);
const { isA } = require(`@langlois/is-a`);

describe(`checkDateType function`, () => {
	beforeEach(() => {
		isA.string = jest.fn(() => true);
	});

	test(`exists`, () => {
		expect(checkDateType).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(checkDateType).toBeInstanceOf(Function);
	});

	test(`doesn't throw an error if the data passed is an instance of a Date`, () => {
		expect(() => {
			checkDateType(`fake-input`, new Date());
		}).not.toThrow();
	});

	describe(`the data passed is not an instance of a Date`, () => {
		beforeEach(() => {
			isA.string.mockReturnValue(false);
		});

		test(`throws a type error`, () => {
			expect(() => {
				checkDateType(`fake-input`, `fake-date`);
			}).toThrow(TypeError);
		});

		test(`the error message is relevant`, () => {
			expect(() => {
				checkDateType(`fake-input`, `fake-date`);
			}).toThrow(`Field fake-input should be an instance of a Date. Received "fake-date".`);
		});
	});
});
