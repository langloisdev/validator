const { checkNumberType } = require(`./checkNumberType`);
const { isA } = require(`@langlois/is-a`);

describe(`checkNumberType function`, () => {
	beforeEach(() => {
		isA.number = jest.fn(() => true);
	});

	test(`exists`, () => {
		expect(checkNumberType).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(checkNumberType).toBeInstanceOf(Function);
	});

	test(`doesn't throw an error if the data passed is an instance of a Number`, () => {
		expect(() => {
			checkNumberType(`fake-input`, 123);
		}).not.toThrow();
	});

	describe(`the data passed is not an instance of a Number`, () => {
		beforeEach(() => {
			isA.number.mockReturnValue(false);
		});

		test(`throws a type error`, () => {
			expect(() => {
				checkNumberType(`fake-input`, `123abc`);
			}).toThrow(TypeError);
		});

		test(`the error message is relevant`, () => {
			expect(() => {
				checkNumberType(`fake-input`, `123abc`);
			}).toThrow(`Field fake-input should be an instance of a Number. Received "123abc".`);
		});
	});
});
