const { checkBooleanType } = require(`./checkBooleanType`);
const { isA } = require(`@langlois/is-a`);

describe(`checkBooleanType function`, () => {
	beforeEach(() => {
		isA.boolean = jest.fn(() => true);
	});

	test(`exists`, () => {
		expect(checkBooleanType).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(checkBooleanType).toBeInstanceOf(Function);
	});

	test(`doesn't throw an error if the data passed is an instance of a Boolean`, () => {
		expect(() => {
			checkBooleanType(`fake-input`, true);
		}).not.toThrow();
	});

	describe(`the data passed is not an instance of a Boolean`, () => {
		beforeEach(() => {
			isA.boolean.mockReturnValue(false);
		});

		test(`throws a type error`, () => {
			expect(() => {
				checkBooleanType(`fake-input`, `boolean`);
			}).toThrow(TypeError);
		});

		test(`the error message is relevant`, () => {
			expect(() => {
				checkBooleanType(`fake-input`, `boolean`);
			}).toThrow(`Field fake-input should be an instance of a Boolean. Received "boolean".`);
		});
	});
});
