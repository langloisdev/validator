const { checkUuidType } = require(`./checkUuidType`);
const { isA } = require(`@langlois/is-a`);

describe(`checkUuidType function`, () => {
	beforeEach(() => {
		isA.string = jest.fn(() => true);
	});

	test(`exists`, () => {
		expect(checkUuidType).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(checkUuidType).toBeInstanceOf(Function);
	});

	test(`doesn't throw an error if the data passed is a UUID`, () => {
		expect(() => {
			checkUuidType(`fake-input`, `096cfbb7-f226-4ddc-befa-db202f986cfa`);
		}).not.toThrow();
	});

	describe(`the data passed is not a UUID`, () => {
		beforeEach(() => {
			isA.string.mockReturnValue(false);
		});

		test(`throws a type error`, () => {
			expect(() => {
				checkUuidType(`fake-input`, 123);
			}).toThrow(TypeError);
		});

		test(`the error message is relevant`, () => {
			expect(() => {
				checkUuidType(`fake-input`, 123);
			}).toThrow(`Field fake-input should be an instance of a UUID. Received "123".`);
		});
	});
});
