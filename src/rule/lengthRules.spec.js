const { TooShortError, TooLongError } = require(`../error.js`);
const { isValueTooShort, isValueTooLong } = require(`./lengthRules.js`);

describe(`validate if an input is too short`, () => {
	describe(`if the input has a min length rule`, () => {
		describe(`the input is longer than the min length`, () => {
			test(`don't throw a TooShortError`, () => {
				expect(() => {
					return isValueTooShort(`testInput`, 5, `hello world`);
				}).not.toThrow(
					TooShortError
				);
			});
		});

		describe(`the input is equal to the min length`, () => {
			test(`don't throw a TooShortError`, () => {
				expect(() => {
					return isValueTooShort(`testInput`, 5, `12345`);
				}).not.toThrow(
					TooShortError
				);
			});
		});

		describe(`the input is less than the min length`, () => {
			test(`throw a TooShortError`, () => {
				expect(() => {
					return isValueTooShort(`testInput`, 5, `1234`);
				}).toThrow(
					new TooShortError(`The testInput field should be at least 5 characters long.`)
				);
			});
		});
	});

	describe(`if the input doesn't have a min length rule`, () => {
		test(`don't throw a TooShortError`, () => {
			expect(() => {
				return isValueTooShort(`testInput`, undefined, `hello world`);
			}).not.toThrow(
				TooShortError
			);
		});
	});
});

describe(`validate if an input is too long`, () => {
	describe(`if the input has a max length rule`, () => {
		describe(`the input is longer than the max length`, () => {
			test(`throw a TooLongError`, () => {
				expect(() => isValueTooLong(`testInput`, 5, `hello world`)
				).toThrow(
					new TooLongError(`The testInput field can't be longer than 5 characters.`)
				);
			});
		});

		describe(`the input is equal to the max length`, () => {
			test(`don't throw a TooLongError`, () => {
				expect(() => {
					return isValueTooLong(`testInput`, 5, `12345`);
				}).not.toThrow(
					TooLongError
				);
			});
		});

		describe(`the input is less than the max length`, () => {
			test(`throw a TooLongError`, () => {
				expect(() => {
					return isValueTooLong(`testInput`, 5, `1234`);
				}).not.toThrow(
					TooLongError
				);
			});
		});
	});

	describe(`if the input doesn't have a max length rule`, () => {
		test(`don't throw a TooLongError`, () => {
			expect(() => {
				return isValueTooLong(`testInput`, undefined, `hello world`);
			}).not.toThrow(
				TooLongError
			);
		});
	});
});
