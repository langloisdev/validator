const { isA } = require(`@langlois/is-a`);
const { RequiredError } = require(`../error.js`);

/**
 * Throws a required error if a required field is undefined.
 *
 * @param {string} input - The name of the input.
 * @param {boolean} isRequired - A flag representing if the input is required.
 * @param {*} data - The submitted data associated with the input.
 *
 * @throws {RequiredError} - The field is required and is undefined.
 *
 * @example
 *
 * try {
 *     checkIsRequired(`inputName`, true, `some data`);
 * } catch (error) {
 *     // handle error if a required field is undefined
 * }
 */
module.exports.checkIsRequired = (input, isRequired, data) => {
	if (isRequired && isA.undefined(data)) {
		throw new RequiredError(`Missing required field: ${input}.`);
	}
};
