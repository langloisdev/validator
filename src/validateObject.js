const { isA } = require(`@langlois/is-a`);
const { isValueAllowed } = require(`./rule/isValueAllowed.js`);
const { checkIsRequired } = require(`./rule/isRequiredRule.js`);
const { validateTypeRules } = require(`./rule/typeRules.js`);
const { validateTypes } = require(`./validateTypes.js`);
const { isValueTooShort, isValueTooLong } = require(`./rule/lengthRules.js`);

/**
 * Validates that the parameters of an object conform to the rules passed.
 *
 * @memberof validator
 *
 * @param {Object} rules
 * @param {Object} rules.paramName - A set of rules for the object. "paramName" should match the key in the object that the rules applies to
 * @param {Array} [rules.paramName.allowedValues] - A list of values that the given input must be part of
 * @param {Number} [rules.paramName.maxLength] - The maximum length an input can be
 * @param {Number} [rules.paramName.minLength] - The minimum length an input can be
 * @param {Boolean} [rules.paramName.required] - A flag that determines if the input must be defined
 * @param {String} rules.paramName.type - The data type of the input
 * @param {Object} data - The object to be evaluated
 *
 * @example
 *
 * const rules = {
 *     password: {
 *         maxLength: 64,
 *         minLength: 7,
 *         required: true,
 *         type: `text`
 *     },
 *     username: {
 *         maxLength: 16,
 *         minLength: 3,
 *         required: true,
 *         type: `text`
 *     }
 * };
 *
 * const data = {
 *     password: `123-abc-set-go`,
 *     username: `franky`
 * }
 *
 * try {
 *     validateObject(rules, data);
 * } catch (error) {
 *     // handle error
 * }
 */
module.exports.validateObject = (ruleSet, data) => {
	for (const [key, rules] of Object.entries(ruleSet)) {
		const value = data[key];

		checkIsRequired(key, rules.required, value);

		if (isA.undefined(value)) return;

		// Standard constructors (String, Function, Array, etc) are functions
		if (isA.string(rules.type) || isA.function(rules.type)) {
			validateTypeRules(key, rules.type, value);
		} else if (isA.array(rules.type)) {
			validateTypes(key, rules.type, value);
		} else {
			throw new TypeError(`Invalid rule type.`);
		}

		isValueTooShort(key, rules.minLength, value);

		isValueTooLong(key, rules.maxLength, value);

		if (rules.allowedValues && rules.allowedValues.length || rules.type === `enum`) {
			isValueAllowed(key, rules.allowedValues, value);
		}
	}
};
