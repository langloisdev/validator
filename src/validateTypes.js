const { validateTypeRules } = require(`./rule/typeRules.js`);

module.exports.validateTypes = (input, types, data) => {
	for (let i = 0; i < types.length; i++) {
		try {
			validateTypeRules(input, types[i], data);

			return;
		} catch (error) {
			if (i === types.length - 1) {
				throw new TypeError(
					`The data "${data}" doesn't match one of the types provided "${types.join(`, `)}".`
				);
			}
		}
	}
};
