const { RequiredError, TooLongError, TooShortError } = require(`./error.js`);

describe(`required error`, () => {
	test(`exists`, () => {
		expect(RequiredError).toBeDefined();
	});

	test(`is an Error class`, () => {
		const error = new RequiredError();

		expect(error).toBeInstanceOf(Error);
	});

	test(`is a RequiredError type`, () => {
		const error = new RequiredError();

		expect(error).toBeInstanceOf(RequiredError);
	});

	test(`has the name "RequiredError"`, () => {
		const error = new RequiredError();

		expect(error.name).toBe(`RequiredError`);
	});

	describe(`the stack`, () => {
		let captureStackTrace;

		beforeEach(() => {
			captureStackTrace = jest.fn();
		});

		test(`captures the stack if the ability to do so exists within the engine`, () => {
			Error.captureStackTrace = captureStackTrace;

			new RequiredError();

			expect(captureStackTrace).toHaveBeenCalledTimes(1);
		});

		test(`doesn't capture the stack if the ability to do so isn't possible within the engine`, () => {
			Error.captureStackTrace = false;

			new RequiredError();

			expect(captureStackTrace).toHaveBeenCalledTimes(0);
		});
	});
});

describe(`too short error`, () => {
	test(`exists`, () => {
		expect(TooShortError).toBeDefined();
	});

	test(`is an Error class`, () => {
		const error = new TooShortError();

		expect(error).toBeInstanceOf(Error);
	});

	test(`is a TooShortError type`, () => {
		const error = new TooShortError();

		expect(error).toBeInstanceOf(TooShortError);
	});

	test(`has the name "TooShortError"`, () => {
		const error = new TooShortError();

		expect(error.name).toBe(`TooShortError`);
	});

	describe(`the stack`, () => {
		let captureStackTrace;

		beforeEach(() => {
			captureStackTrace = jest.fn();
		});

		test(`captures the stack if the ability to do so exists within the engine`, () => {
			Error.captureStackTrace = captureStackTrace;

			new TooShortError();

			expect(captureStackTrace).toHaveBeenCalledTimes(1);
		});

		test(`doesn't capture the stack if the ability to do so isn't possible within the engine`, () => {
			Error.captureStackTrace = false;

			new TooShortError();

			expect(captureStackTrace).toHaveBeenCalledTimes(0);
		});
	});
});

describe(`too long error`, () => {
	test(`exists`, () => {
		expect(TooLongError).toBeDefined();
	});

	test(`is an Error class`, () => {
		const error = new TooLongError();

		expect(error).toBeInstanceOf(Error);
	});

	test(`is a TooLongError type`, () => {
		const error = new TooLongError();

		expect(error).toBeInstanceOf(TooLongError);
	});

	test(`has the name "TooLongError"`, () => {
		const error = new TooLongError();

		expect(error.name).toBe(`TooLongError`);
	});

	describe(`the stack`, () => {
		let captureStackTrace;

		beforeEach(() => {
			captureStackTrace = jest.fn();
		});

		test(`captures the stack if the ability to do so exists within the engine`, () => {
			Error.captureStackTrace = captureStackTrace;

			new TooLongError();

			expect(captureStackTrace).toHaveBeenCalledTimes(1);
		});

		test(`doesn't capture the stack if the ability to do so isn't possible within the engine`, () => {
			Error.captureStackTrace = false;

			new TooLongError();

			expect(captureStackTrace).toHaveBeenCalledTimes(0);
		});
	});
});
