/**
 * An error that takes care of capturing the stack.
 * @class
 * @param {string} message - The error message.
 *
 * @example
 *
 * const error = new ValidatorError(`error message`);
 */
class ValidatorError extends Error {
	constructor (message) {
		super(message);

		if (Error.captureStackTrace) {
			Error.captureStackTrace(this, module.exports.RequiredError);
		}
	}
}

/**
 * A required input is undefined.
 * @class
 * @param {string} message - The error message.
 *
 * @example
 *
 * const error = new RequiredError(`error message`);
 */
module.exports.RequiredError = class RequiredError extends ValidatorError {
	constructor (message) {
		super(message);

		this.name = `RequiredError`;
	}
};

/**
 * An input is longer than its maximum allowable length.
 * @class
 * @param {string} message - The error message.
 *
 * @example
 *
 * const error = new TooLongError(`error message`);
 */
module.exports.TooLongError = class TooLongError extends ValidatorError {
	constructor (message) {
		super(message);

		this.name = `TooLongError`;
	}
};

/**
 * An input is shorter than its minimum allowable length.
 * @class
 * @param {string} message - The error message.
 *
 * @example
 *
 * const error = new TooShortError(`error message`);
 */
module.exports.TooShortError = class TooShortError extends ValidatorError {
	constructor (message) {
		super(message);

		this.name = `TooShortError`;
	}
};
