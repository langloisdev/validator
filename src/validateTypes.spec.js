jest.mock(`./rule/typeRules.js`);

const { validateTypeRules } = require(`./rule/typeRules.js`);
const { validateTypes } = require(`./validateTypes.js`);

describe(`validate types function`, () => {
	beforeEach(() => {
		validateTypeRules.mockImplementation(() => undefined);
	});

	test(`exists`, () => {
		expect(validateTypes).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(validateTypes).toBeInstanceOf(Function);
	});

	test(`doesn't throw if the data matches one of the expected types`, () => {
		expect(() => {
			validateTypes(`fake-input`, [`string`, `number`], `fake data`);
		}).not.toThrow();
	});

	describe(`if the data doesn't match any of the expected types`, () => {
		beforeEach(() => {
			validateTypeRules.mockImplementation(() => {
				throw new Error();
			});
		});

		test(`throws a TypeError`, () => {
			expect(() => {
				validateTypes(`fake-input`, [`string`, `number`], false);
			}).toThrow(TypeError);
		});

		test(`throws a useful message`, () => {
			expect(() => {
				validateTypes(`fake-input`, [`string`, `number`], false);
			}).toThrow(`The data "false" doesn't match one of the types provided "string, number".`);
		});
	});
});
