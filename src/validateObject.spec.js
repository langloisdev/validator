jest.mock(`./rule/isRequiredRule.js`);
jest.mock(`./rule/isValueAllowed.js`);
jest.mock(`./rule/typeRules.js`);
jest.mock(`./rule/lengthRules.js`);
jest.mock(`./validateTypes.js`);

const { checkIsRequired } = require(`./rule/isRequiredRule.js`);
const { validateTypeRules } = require(`./rule/typeRules.js`);
const { validateObject } = require(`./validateObject.js`);
const { validateTypes } = require(`./validateTypes.js`);
const { isValueTooShort, isValueTooLong } = require(`./rule/lengthRules.js`);
const { isValueAllowed } = require(`./rule/isValueAllowed.js`);

describe(`validate object`, () => {
	beforeEach(() => {
		checkIsRequired.mockImplementation(() => undefined);
		isValueAllowed.mockImplementation(() => undefined);
		validateTypeRules.mockImplementation(() => undefined);
		isValueTooShort.mockImplementation(() => undefined);
		isValueTooLong.mockImplementation(() => undefined);
	});

	test(`exists`, () => {
		expect(validateObject).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(validateObject).toBeInstanceOf(Function);
	});

	describe(`for each parameter`, () => {
		let data;
		let rules;

		beforeEach(() => {
			data = {
				test: `abc`
			};

			rules = {
				test: {
					maxLength: 64,
					minLength: 3,
					required: true,
					type: `text`
				}
			};
		});

		test(`check if the parameter is required`, () => {
			validateObject(rules, data);

			expect(checkIsRequired).toHaveBeenCalledTimes(1);

			expect(checkIsRequired).toHaveBeenCalledWith(`test`, true, `abc`);
		});

		describe(`if the parameter is defined`, () => {
			test(`validate the type rules`, () => {
				validateObject(rules, data);

				expect(validateTypeRules).toHaveBeenCalledTimes(1);

				expect(validateTypeRules).toHaveBeenCalledWith(`test`, `text`, `abc`);
			});

			test(`validate the type rules if type is a constructor`, () => {
				rules.test.type = String;
				validateObject(rules, data);

				expect(validateTypeRules).toHaveBeenCalledTimes(1);

				expect(validateTypeRules).toHaveBeenCalledWith(`test`, String, `abc`);
			});

			test(`validate the too short rule`, () => {
				validateObject(rules, data);

				expect(isValueTooShort).toHaveBeenCalledTimes(1);

				expect(isValueTooShort).toHaveBeenCalledWith(`test`, 3, `abc`);
			});

			test(`validate the too long rule`, () => {
				validateObject(rules, data);

				expect(isValueTooLong).toHaveBeenCalledTimes(1);

				expect(isValueTooLong).toHaveBeenCalledWith(`test`, 64, `abc`);
			});

			describe(`the validate type rules is an array of options`, () => {
				beforeEach(() => {
					rules.test.type = [
						`string`,
						`number`
					];
				});

				test(`validate types is called`, () => {
					validateObject(rules, data);

					expect(validateTypes).toHaveBeenCalledTimes(1);

					expect(validateTypes).toHaveBeenCalledWith(`test`, [
						`string`,
						`number`
					], `abc`);
				});
			});

			describe(`the validate type rules is not an array or a string`, () => {
				beforeEach(() => {
					rules.test.type = {};
				});

				test(`throw a TypeError`, () => {
					expect(() => {
						validateObject(rules, data);
					}).toThrow(TypeError);
				});

				test(`throw a useful message`, () => {
					expect(() => {
						validateObject(rules, data);
					}).toThrow(`Invalid rule type.`);
				});
			});

			describe(`validate the allowed values if`, () => {
				test(`there's allowed values`, () => {
					rules.test.allowedValues = [`at least one`];
					validateObject(rules, data);

					expect(isValueAllowed).toHaveBeenCalledTimes(1);
					expect(isValueAllowed).toHaveBeenCalledWith(`test`, [`at least one`], `abc`);
				});

				test(`the type is enum`, () => {
					rules.test.type = `enum`;
					validateObject(rules, data);

					expect(isValueAllowed).toHaveBeenCalledTimes(1);
					expect(isValueAllowed).toHaveBeenCalledWith(`test`, undefined, `abc`);
				});
			});

			describe(`don't validate the allowed values if`, () => {
				test(`there's no allowed values and the type is not enum`, () => {
					rules.test.allowedValues = [];
					validateObject(rules, data);

					expect(isValueAllowed).toHaveBeenCalledTimes(0);

					rules.test.allowedValues = undefined;
					validateObject(rules, data);

					expect(isValueAllowed).toHaveBeenCalledTimes(0);
				});
			});
		});

		describe(`if the parameter is not defined`, () => {
			beforeEach(() => {
				data = {};
			});

			test(`don't validate the type rules`, () => {
				validateObject(rules, data);

				expect(validateTypeRules).toHaveBeenCalledTimes(0);
			});

			test(`don't validate the too short rule`, () => {
				validateObject(rules, data);

				expect(isValueTooShort).toHaveBeenCalledTimes(0);
			});

			test(`don't validate the too long rule`, () => {
				validateObject(rules, data);

				expect(isValueTooLong).toHaveBeenCalledTimes(0);
			});

			test(`don't validate the is value allowed rule`, () => {
				validateObject(rules, data);

				expect(isValueAllowed).toHaveBeenCalledTimes(0);
			});
		});
	});
});
