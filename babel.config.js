module.exports = api => {
	api.cache(true);

	return {
		plugins: [],
		presets: [
			[`@babel/preset-env`, {
				targets: {
					node: true
				}
			}]
		]
	};
};
