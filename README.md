# Validator Library

Validates if an Object adheres to certain restrictions.

## Installation
```js
npm i -S @langlois/validator
```

## Usage
```js
const { validator } = require(`@langlois/validator`);

const rules = {
	email: {
		required: true,
		type: `email`
	},
	password: {
		max: 64,
		min: 7,
		required: true,
		type: `text`
	}
	username: {
		max: 24,
		min: 3,
		required: true,
		type: `text`
	}
};

const data = {
	email: `francis@langlois.dev`,
	password: `hard-to-guess-password`,
	username: `Franky`
};

const isValid = validator.validateObject(rules, data);
```

## API

### validateObject (rules, data)

Validates whether the data adheres to the rules passed.
