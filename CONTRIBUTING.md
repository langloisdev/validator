# Contributing Guide

## Styleguide
### Git Messages

- use the present tense
- use the imperative mood
- keep commit messages short and succint
- start your commit message with an appropriate emoji:
	- :art: cleaning/re-organizing code
	- :bug: fixing a bug
	- :arrow_down: downgrading dependencies
	- :arrow_up: upgrading dependencies
	- :white_check_mark: adding tests
	- :shirt: fixing linter errors/warnings
	- :link: CI
	- :zap: performance
	- :notebook: documentation
	- :skull: removing code/file
	- :lock: security
	- :star: feature

### Documentation
We use Markdown for our documentation.

### JavaScript
We use the [Recommended ESLint rules](https://eslint.org/docs/rules/) as well as our estlint rules that follow [our styleguide](https://gitlab.com/langlois.dev/company-standards/blob/828d98a159f2dab98200b0368bf9ed04c8977b39/styleguides/javascript.md).
