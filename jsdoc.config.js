module.exports = {
	recurseDepth: 10,
	source: {
		include: [
			`./index.js`,
			`./src/`
		],
		exclude: [
			`./node_modules/`
		],
		includePattern: `.+\\.js(doc)?$`,
		excludePattern: `(^|\\/|\\\\)_|.+.spec.`
	},
	tags: {
		allowUnknownTags: true,
		dictionaries: [
			`closure`,
			`jsdoc`
		]
	},
	templates: {
		cleverLinks: true,
	},
	opts: {
		template: `./node_modules/clean-jsdoc-theme`,
		theme_opts: {
			title: `Object Validator`,
			search: true
		}
	}
};
