import { validator } from '../index.js';

export const authenticate = validator.validateObject;

export {
	validator as default,
	validateObject
};
