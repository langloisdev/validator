const { validateObject } = require(`./src/validateObject.js`);

/** @namespace validator */
module.exports.validator = {
	validateObject
};
